# dell-xps15-9560

This repo contains a working an non-minimal version of .config file for building a Gentoo kernel 
for the Dell XPS 15 9560 (2017) with Kabylake processor. It is designed to work with systemd profile and also nicely interacts with Dell Thunderbolt (USB-C) Dock WD-15.

In order to save power one can use the supplied systemd service (see etc directory).

